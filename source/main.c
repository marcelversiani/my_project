#include <stdio.h>
#include "dice.h"

int main() {

	initializeSeed();
//Ler quantas faces terá o dado
	int faces;
	printf("How many faces will your dice have? ");
	scanf("%d", &faces);
//Rolar o dado com 'faces' lados
	printf("Let's roll the dice: %d\n", rollDice(faces));

	return 0;
}
